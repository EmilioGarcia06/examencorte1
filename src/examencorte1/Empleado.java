/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examencorte1;

public abstract class Empleado {
    protected int numEmpleado;
    protected String Nombre;
    protected String Domicilio;
    protected float pagoDiario;
    protected int diasTrabajados;

    public Empleado(int numEmpelado, String Nombre, String Domicilio, float pagoDiario, int diasTrabajados) {
        this.numEmpleado = numEmpelado;
        this.Nombre = Nombre;
        this.Domicilio = Domicilio;
        this.pagoDiario = pagoDiario;
        this.diasTrabajados = diasTrabajados;
    }
  public Empleado() {
        this.numEmpleado = 0;
        this.Nombre = "";
        this.Domicilio = "";
        this.pagoDiario = 0.0f;
        this.diasTrabajados = 0;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
  public float calcualrTotal(){
      float pago = 0; 
      pago = this.pagoDiario * this.diasTrabajados;
      return pago;        
  }
//    public float calcularDescuento(){
//    float paga = 0;
//    paga = pago *;
//}
}
    
